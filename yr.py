from datetime import datetime

def days_between(d1, d2):
    d1 = datetime.strptime(d1, "%Y-%m-%d")
    d2 = datetime.strptime(d2, "%Y-%m-%d")
    print(abs((d2 - d1)))
    return abs((d2 - d1).days)
days_between('1994-09-23', '1999-09-24')
