import re
import difflib
from urlextract import URLExtract
# from google.cloud import language
from commonregex import CommonRegex
# from nltk import word_tokenize, pos_tag, ne_chunk
# import spacy
# import nltk
from difflib import get_close_matches
#from nltk.parse.stanford import StanfordDependencyParser

class ContactInfo:
    def __init__(self, document):
        # print(document)
        # search for phone number
        # search = re.compile('^(\d{5}\-\d{5}$)', re.MULTILINE)
        # match = re.search(search, document)
        # pull out numbers (get rid of - and stuff)
        doc=re.compile('(\+\d{2}\d{5}\-\d{5}|\+\d{2}\-\d{3}\-\d{3}-\d{4}|\+\d{2}\-\d{2}\-\d{4}-\d{4}|\+\d{2} \d{5} \d{5}|\+\d{2} \d{4} \d{3} \d{3}|\d{3} \d{4} \d{4}|\+\d{2}\-\d{10}|\+\d{2} \d{10}|\d{10}|\d{5} \d{5}|\+\d{2} \d{2} \d{4} \d{4}|\+d{2} [0-9]{3} \d{3}\-\d{4}|\+\d{2} \d{3} \d{3} \d{4}|\d{3}\-\d{8}|\+\d{2}\-\d+\-\d+|\d{3}\-\d{4} \d{4}|\d{4} \d{3} \d{3}|\+\d{2} \d{2} \d{3} \d{5}|\+\d{2}\-\d{3}\-\d{3}\-\d{4}|\+[0-9]{2} [0-9]{4} [0-9]{6})')
        match = doc.findall(document)
        print("......:",match)
        self.m_phone= ''
        if  len(match)>=1:
            self.m_phone = match[0] # turn array into string      
        # number = re.compile('(?:(?:\+?([1-9]|[0-9][0-9]|[0-9][0-9][0-9])\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([0-9][1-9]|[0-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?')
        # mobile=number.findall(document)
        # print("mobile_number:",mobile)
        # pull email
        search = re.compile('([a-zA-Z]+\@[a-zA-Z-]+\.[a-zA-Z]+|[a-zA-Z]+\@[a-zA-Z]+\.[a-zA-Z]+\.in$|[a-zA-z]+.[a-zA-Z0-9]+\@[a-zA-Z]+\.[a-zA-Z]+|[a-zA-z]+.[a-zA-Z0-9]+\@[a-zA-Z]+\.[a-zA-Z]+\.[a-zA-Z]+)')
        match = search.findall(document)
        self.m_email=''
        mail=''
        if len(match)>=1:
            print("/email:",match)
            mail =match[0]
            self.m_email = match[0]      
        #search = re.compile('^\\w+\\s+\\w+$', re.MULTILINE)
        #match = re.findall(search, document)
        #match = [x.lower() for x in match]
       # username = self.m_email[:self.m_email.find('@')]
        #matches = difflib.get_close_matches(username, match)
       # print("names:",matches)
        self.m_name = ''
        site=re.compile('(?:www\.[a-zA-Z]+\.[a-zA-Z]+\.[a-zA-Z]+|www\.[a-zA-Z]+\.[a-zA-Z]+)')#|?:www\.[a-zA-Z]+\.\w+|?:www\.[a-zA-Z]+\.\w+)')
        web=site.findall(document)
        print("sites:",web)
        self.m_website = ' '
        extractor = URLExtract()
        urls = extractor.find_urls(document)
        urls=[x for x in urls if '@' not in x]
        #print("urls...:",urls[0])
        #pull name
        # mails = extractor.find_urls(document)
        # mails=[x for x in mails if '@' in x]
        # print("mails:",mails)
        # self.m_email=''
        # mail=''
        # if len(mails)>=1:
        #     self.m_email=mails[0]
        #     mail=self.m_email
        site =''
        if len(urls)>=1:
            self.m_website = urls[0]
            site=self.m_website
        location =re.compile('(\w*)\s+([A-Z]{3}\s+\d[A-Z]{2})')  
        address=location.findall(document)
        print("............//////////:",address)  
        print(mail)
        c=''
        if mail !='':
            print("....")
            bc=mail.index('@')
            a=mail[bc+1:]
            print("[[[",a)
            b=a.index('.')
            c=(a[:b])
            print("company_name...:",c)
        self.m_companyname=''
        if c!='':
            if c not in ['gmail','yahoo']:
                self.m_companyname=c
        try:
            if site!='':
                alpha=site.index('.')
                self.m_companyname=''
                beta=site[alpha+1:]
                print(";;;;;;;;;;;;;",beta)
                cab=beta.index('.')
                gamma=(beta[:cab])
                print("company_names.....:",gamma)
                self.m_companyname=gamma
            

           
        except:
            if mail !='':
                print("....")
                bc=mail.index('@')
                a=mail[bc+1:]
                print("[[[",a)
                b=a.index('.')
                c=(a[:b])
                print("company_name...:",c)
        
        
        # block=str(document).split('\n')
        # comp_names=''
        # if c!='':
        #     comp_names=[c]
        # entry=' '
        # for y in block[::-1]:
        #     a=get_close_matches(y,comp_names)
        #     print("////////////:",a)
        #     if len(a)==1:
        #         entry=a[0]
        #         print("no of entries:",a[0])
 #       nlp = spacy.load('en')

#        doc = nlp(document)

        #print("...,,,,,..:",[ent for ent in doc.ents])
        #print (ne_chunk(pos_tag(word_tokenize(document))))
        #dep_parser=StanfordDependencyParser(model_path="/home/caratred/Downloads/Stanford-parser-python-r22186.tar.gz")
        #print ([parse.tree() for parse in dep_parser.raw_parse(document)])
    @property
    def getName(self):
        return self.m_name

    @property
    def getPhoneNumber(self):
        return self.m_phone

    @property
    def getEmailAddress(self):
        return self.m_email
    @property
    def getWebsite(self):
        return self.m_website
    @property
    def getcompanyname(self):
        return self.m_companyname
