import nltk
# from nameparser import HumanName
from nltk.corpus import wordnet
from nltk.tag.stanford import StanfordNERTagger
nltk.download('wordnet')

person_list = []
person_names=person_list
def get_human_names(text):
    tokens = nltk.tokenize.word_tokenize(text)
    pos = nltk.pos_tag(tokens)
    sentt = nltk.ne_chunk(pos, binary = False)

    person = []
    name = ""
    for subtree in sentt.subtrees(filter=lambda t: t.label() == 'PERSON'):
        for leaf in subtree.leaves():
            person.append(leaf[0])
        if len(person) > 1: #avoid grabbing lone surnames
            for part in person:
                name += part + ' '
            if name[:-1] not in person_list:
                person_list.append(name[:-1])
            name = ''
        person = []
#     print (person_list)

# text = """

# Some economists have responded positively to Bitcoin, including 
# Francois R. Velde, senior economist of the Federal Reserve in Chicago 
# who described it as "an elegant solution to the problem of creating a 
# digital currency." In November 2013 Richard Branson announced that 
# Virgin Galactic would accept Bitcoin as payment, saying that he had invested 
# in Bitcoin and found it "fascinating how a whole new global currency 
# has been created", encouraging others to also invest in Bitcoin.
# Other economists commenting on Bitcoin have been critical. 
# Economist Paul Krugman has suggested that the structure of the currency 
# incentivizes hoarding and that its value derives from the expectation that 
# others will accept it as payment. Economist Larry Summers has expressed 
# a "wait and see" attitude when it comes to Bitcoin. Nick Colas, a market 
# strategist for ConvergEx Group, has remarked on the effect of increasing 
# use of Bitcoin and its restricted supply, noting, "When incremental 
# adoption meets relatively fixed supply, it should be no surprise that 
# prices go up. And that’s exactly what is happening to BTC prices."
# """
# text1 ="WAJID AZHARUDDIN.D Vice-president Groupera treg orice +919100119347 azhar@thegroupera.com"
# text1 = "Jaswanth Kumar Co-Founder Inquilex LLP Ittina anai apts, D-107 Kempepura main road, Bellandur Bangalore-560037 Inquilex.com contact@inquilex.com 9966733383"

        
# names = get_human_names(text1)
# for person in person_list:
#     person_split = person.split(" ")
#     for name in person_split:
#         if wordnet.synsets(name):
#             if(name in person):
#                 person_names.remove(person)
#                 break

# print(person_names)
# name =HumanName(text1)
# print(name.as_dict())
import os
os.environ['STANFORD_MODELS'] = '/home/caratred01/standford/stanford-corenlp-caseless-2015-04-20-models/edu/stanford/nlp/models/ner'
stanford_dir = '/home/caratred01/standford'
os.environ['CLASSPATH'] = os.path.join(stanford_dir, 'stanford-ner.jar')
# modelfile = os.path.join('/home/caratred01/stanford', 'stanford-corenlp-caseless-2015-04-20-models/edu/stanford/nlp/models/ner')
stanford_classifier = '/home/caratred01/standford/stanford-corenlp-caseless-2015-04-20-models/edu/stanford/nlp/models/ner/english.all.3class.caseless.distsim.crf.ser.gz'
# st = StanfordNERTagger(model_filename=os.environ['STANFORD_MODELS'], path_to_jar=jarfile)
def get_continuous_chunks(tagged_sent):
    continuous_chunk = []
    current_chunk = []

    for token, tag in tagged_sent:
        if tag != "O":
            current_chunk.append((token, tag))
        else:
            if current_chunk: # if the current chunk is not empty
                continuous_chunk.append(current_chunk)
                current_chunk = []
    # Flush the final current_chunk into the continuous_chunk, if any.
    if current_chunk:
        continuous_chunk.append(current_chunk)
    return continuous_chunk

def name_parser(text1):
    stner = StanfordNERTagger(stanford_classifier)
    tagged_sent = stner.tag(text1.split())

    named_entities = get_continuous_chunks(tagged_sent)
    named_entities_str_tag = [(" ".join([token for token, tag in ne]), ne[0][1]) for ne in named_entities]



    print(named_entities_str_tag)
    abc = {value:key for key,value in named_entities_str_tag}
    name = ''
    company = ''

    for key,value in abc.items():
        
        if key == 'PERSON':
            print(value)
            name = value
      
        if key == 'ORGANIZATION':
            company = value
    return {"name":name,"company":company}
# name_parser(text1)