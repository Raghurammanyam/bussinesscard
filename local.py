import requests
import json
import collections
def local():

   # data=requests.get(apicall)
    
    data=[
        {
            "id": 1,
            "open": 0,
            "closed": 1,
            "shutdown_date_time": 'null',
            "case_owner": "Jinhee Na                                         ",
            "product_id": "PureCell 400 - 9850",
            "case_number": 65423,
            "shutdown_code": "TE910 - Reformer Burner Exhaust Temperature (HEX911 Outlet/HEX910 Inlet)",
            "outage_category": "Failed Start",
            "outage_type": "Powerplant Induced Shutdown",
            "load_hours_attimeof_shutdown": 3144,
            "restart_date": "2019-02-26T02:30:00Z",
            "root_cause_category": "FPS",
            "root_cause": "FT140",
            "resolution": "Adjusted FT140 location.",
            "pplt_number": 9850,
            "customer_location": 'null',
            "customer_region": "0",
            "sd_category": "FPS",
            "configurations": "HE/HP (GEN II)"
        },
        {
            "id": 2,
            "open": 1,
            "closed": 0,
            "shutdown_date_time": 'null',
            "case_owner": "Rodolphe Rivaux                                   ",
            "product_id": "PureCell 400 - 9507",
            "case_number": 62835,
            "shutdown_code": "<NORMAL_SHUTDOWN> - Normal Shutdown Signal from LOI",
            "outage_category": "Failed Start",
            "outage_type": "Powerplant Induced Shutdown",
            "load_hours_attimeof_shutdown": 38528,
            "restart_date": "2018-12-21T20:19:00Z",
            "root_cause_category": 'null',
            "root_cause": 'null',
            "resolution": 'null',
            "pplt_number": 9507,
            "customer_location": "Pratt & Whitney",
            "customer_region": "East",
            "sd_category": "Manual S/D",
            "configurations": "Gen I Legacy"
        },
        {
            "id": 3,
            "open": 0,
            "closed": 1,
            "shutdown_date_time": "2017-08-13T06:00:00Z",
            "case_owner": "Changkyeom Kim                                    ",
            "product_id": "PureCell 400 - 9652",
            "case_number": 43479,
            "shutdown_code": "<Shutdown> - Shutdown from Operations Display (ET)",
            "outage_category": "Outage",
            "outage_type": "Powerplant Induced Shutdown",
            "load_hours_attimeof_shutdown": 4357,
            "restart_date": "2017-09-20T03:55:00Z",
            "root_cause_category": "ESM",
            "root_cause": "MOV Fuse",
            "resolution": "Rewiring W0616 and 0874",
            "pplt_number": 9652,
            "customer_location": "Busan Green Energy",
            "customer_region": "Korea",
            "sd_category": "Manual S/D",
            "configurations": "Gen I Super Module"
        },
        {
            "id": 4,
            "open": 0,
            "closed": 1,
            "shutdown_date_time": "2017-08-17T07:13:00Z",
            "case_owner": "Hyunchul Ko                                       ",
            "product_id": "PureCell 400 - 9733",
            "case_number": 46951,
            "shutdown_code": "<NORMAL_SHUTDOWN> - Normal Shutdown Signal from LOI",
            "outage_category": "Outage",
            "outage_type": "Powerplant Induced Shutdown",
            "load_hours_attimeof_shutdown": 2250,
            "restart_date": "2018-01-30T22:29:00Z",
            "root_cause_category": "FPS",
            "root_cause": "REF300",
            "resolution": "Severe corriosion and crack on reformer. Reformer is replaced.",
            "pplt_number": 9733,
            "customer_location": "Busan Green Energy - Busan Batch 8",
            "customer_region": "Korea",
            "sd_category": "Manual S/D",
            "configurations": "GEN II"
        },
        {
            "id": 5,
            "open": 0,
            "closed": 1,
            "shutdown_date_time": "2017-08-24T01:14:00Z",
            "case_owner": "Changkyeom Kim                                    ",
            "product_id": "PureCell 400 - 9647",
            "case_number": 42592,
            "shutdown_code": "IP200 - Any PCS (Inverter) Shutdown",
            "outage_category": "Outage",
            "outage_type": "Powerplant Induced Shutdown",
            "load_hours_attimeof_shutdown": 5505,
            "restart_date": "2017-08-26T03:50:00Z",
            "root_cause_category": "ESM",
            "root_cause": "AC150",
            "resolution": "Refrigerant piping was frozen.",
            "pplt_number": 9647,
            "customer_location": "Busan Green Energy",
            "customer_region": "Korea",
            "sd_category": "ESM",
            "configurations": "Gen I Super Module"
        },

        ]
    shutdown_code=[]
    shutdown_date_time=[]
    for x in data:
        for key,values in x.items():
            if key == "shutdown_code":
                shutdown_code.append(values)
            if key == "shutdown_date_time":
                shutdown_date_time.append(values)
    counter=collections.Counter(shutdown_code)
    d = {x:shutdown_code.count(x) for x in shutdown_code}
    print("count shut_down_code:",d)
    x_axis={"shutdown_code":shutdown_code}
    y_axis={"shutdown_date_time":shutdown_date_time}
    print("x_axis:",x_axis)
    print(".........................................")
    print("y_Axis:",y_axis)

    # print("id_list:",id)
    # print("y_axis:",y_axis)
    # print("y_values:",y_values)
    #if data:

     #   context = {'data':data}
      
   # print(data)
    return #render(request, 'jsondata.html', context)

local()                                                 
