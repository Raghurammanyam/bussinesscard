import sys
from BusinessCardParser import BusinessCardParser
from ContactInfo import ContactInfo
import base64
import requests
import io
import os
import json
import re
from info_extract import extract_phone_numbers,extract_email_addresses
# from lib.apiclient import discovery
# from PIL import Image,ImageEnhance,ImageFilter
# import pytesseract
from parsename import name_parser
import nltk
import spacy
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')



def main(image_file):
  
    with open(image_file, 'rb') as image:
        base64_image = base64.b64encode(image.read()).decode()
    url = 'https://vision.googleapis.com/v1/images:annotate?key=AIzaSyAOztXTencncNtoRENa1E3I0jdgTR7IfL0'
    header = {'Content-Type': 'application/json'}
    body = {
        'requests': [{
            'image': {
                'content': base64_image,
            },
            'features': [{
                'type': 'DOCUMENT_TEXT_DETECTION',
                'maxResults': 100,
            }],
            "imageContext":{
            "languageHints":["en-t-iO-handwrit"]
            }
        }]
    }

    response = requests.post(url, headers=header, json=body).json()
    text = response['responses'][0]['textAnnotations'][0]['description'] if len(response['responses'][0]) > 0 else ''
    # print ("kdfjgn:",text)
    # read from file
    # url = 'https://language.googleapis.com/v1/documents:analyzeEntitySentiment?key=AIzaSyD6eDqLgk_HLA7sPHmKEfA1y1DWnvSExmY'
    # header = {'Content-Type': 'application/json' }
    # #lang_service = discovery.build(‘language’, ‘v1beta1’, developerKey=’AIzaSyD6eDqLgk_HLA7sPHmKEfA1y1DWnvSExmY’)
    # data ={
    # 'document':{
    #     'type':'PLAIN_TEXT',
    #     'content':'ram babu'
    # },
    
    
    # 'encodingType':'UTF8'
    # }
    # response = requests.post(url, headers=header, json=data).json()
    # print(response['entities'],"......")
    # print("                                                ")
    # print("..............................................")
    # for x in response['entities']:
    #    # print(x['type'])
    #     if x['type']=='PERSON':
    #         print("name of person:",x['name'])
    #     if x['type']=='ORGANIZATION':
    #          print("name of company:",x['name'])
    #     if x['type']=='LOCATION':
    #         print("location of ..:",x['name'])
            
    # print("                                                           ")
    # print("//////////////////////////////////////////////////////////////")
    ##ret = lang_service.documents().annotateText(body=the_data).execute()
    # #print("..mmm:",ret)
    # img = Image.open(image_file).convert("L")
    # img = img.filter(ImageFilter.SHARPEN())
    # enhancer = ImageEnhance.Brightness(img)
    # out = enhancer.enhance(1.8)
    # nx, ny = out.size
    # #  print("imagesize:",nx,ny)
    # out = out.resize((int(nx*1.5), int(ny*1.5)), Image.ANTIALIAS)
    # #print("size:",out.size)
    # out.save("/home/raghu/copy/passport/Cleaned1.jpeg",quality=94)
    # #out.show()
    # txt = pytesseract.image_to_string(Image.open('/home/raghu/copy/passport/Cleaned1.jpeg'))
    # print("tesseract:",txt)
    extract=text.split('\n')
    # print(extract)
    join_text = ' '.join(x for x in extract)
    print(join_text,"jointext")
    total = text
    extract_phone_numbers(total)
    extract_email_addresses(total)
    print(".............")
    # extract_names(total)
  
    # print(total)

    # parse file
    bcp = BusinessCardParser()
    ci = bcp.getContactInfo(total)

    print("==>\n")
    nlp = spacy.load('en_core_web_md')                                                                                                                  
    cents = nlp(join_text)
    sents=[ee for ee in cents.ents if ee.label_ == 'PERSON'] 
    print(sents,"sentyss")
    dents=[ee for ee in cents.ents if ee.label_ == 'ORG'] 
    print(dents,"dents")
    print(name_parser(join_text))
    # print("Name:", ci.getName)
    print("Phone:", ci.getPhoneNumber)
    print("Email:", ci.getEmailAddress)
    print("site:",ci.getWebsite)
    print("company_name:",ci.getcompanyname)
main('/home/caratred01/Downloads/cards/IMG_20190213_125801 (copy).JPG')